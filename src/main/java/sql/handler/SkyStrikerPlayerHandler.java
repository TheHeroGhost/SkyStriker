package sql.handler;

import de.royalpixels.bedrock.common.orm.sql.DaoHandler;
import de.royalpixels.bedrock.common.orm.sql.SQL;
import sql.table.SkyStrikerPlayer;

import java.util.UUID;

public class SkyStrikerPlayerHandler extends DaoHandler<UUID, SkyStrikerPlayer> {

    public SkyStrikerPlayerHandler(SQL conn) {
        super(conn, "skystriker_players", true);
    }

    public void setName(UUID uuid) {
        SkyStrikerPlayer skyStrikerPlayer = getEntry(uuid);
        skyStrikerPlayer.setUuid(uuid);
        super.cacheEntry(uuid, skyStrikerPlayer);
        super.updateEntry(skyStrikerPlayer);
    }

    public SkyStrikerPlayer getEntry(UUID uuid) {
        createEntry(uuid);
        return super.getEntry("uuid", uuid);
    }

    public SkyStrikerPlayer createEntry(UUID uuid) {
        SkyStrikerPlayer skyStrikerPlayer = createEntryIfNotExists(new SkyStrikerPlayer(uuid));
        super.cacheEntry(uuid, skyStrikerPlayer);
        return skyStrikerPlayer;
    }
}
