package sql.table;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@DatabaseTable(tableName = "skystriker_players")
public class SkyStrikerPlayer {

    @DatabaseField(id = true)
    private UUID uuid;

    public SkyStrikerPlayer() {}

    public SkyStrikerPlayer(UUID uuid) {
        this.uuid = uuid;
    }
}
