package sql;

import de.theheroghost.main.main;
import org.bukkit.Bukkit;
import sql.handler.SkyStrikerPlayerHandler;
import sql.table.SkyStrikerPlayer;

import java.util.UUID;

public class Manager {

    private UUID uuid;
    private String name;

    public Manager(final UUID uuid) {
        this.uuid = uuid;
        main.getStats().put(uuid, this);

    }

    public void load() {
        Bukkit.getScheduler().runTaskAsynchronously(main.getInstance(), () -> {
            SkyStrikerPlayer skyStrikerPlayer = main.getSkyStrikerPlayerHandler().getEntry(uuid);
            uuid = skyStrikerPlayer.getUuid();
        });
    }

    public void set() {
        Bukkit.getScheduler().runTaskAsynchronously(main.getInstance(), () -> {
            SkyStrikerPlayerHandler skyStrikerPlayerHandler = main.getSkyStrikerPlayerHandler();
            skyStrikerPlayerHandler.setName(uuid);
        });
    }

}
