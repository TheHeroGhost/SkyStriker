package de.theheroghost.main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Lobby {

    public static File LobbyFile = new File("plugins/SkyStriker/Arena/",  "Lobbys.yml");
    public static FileConfiguration lobby = YamlConfiguration.loadConfiguration(LobbyFile);

    public static void save() throws IOException {
        try {
            lobby.save(LobbyFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveLobby() {
        try {
            Lobby.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

