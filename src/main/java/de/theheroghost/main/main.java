package de.theheroghost.main;

import de.royalpixels.bedrock.Bedrock;
import de.royalpixels.bedrock.BedrockPlugin;
import de.royalpixels.bedrock.common.orm.sql.SQLConfiguration;
import events.PlayerJoin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import sql.Manager;
import sql.handler.SkyStrikerPlayerHandler;
import sql.table.SkyStrikerPlayer;

import java.io.File;
import java.util.*;

@Getter
public class main extends BedrockPlugin {

    public static List<String> Arenen = new ArrayList<String>();
    private static HashMap<UUID, Manager> stats = new HashMap<>();
    private static SkyStrikerPlayerHandler skyStrikerPlayerHandler;
    private static main instance;

    File configFile = new File("plugins/SkyStriker/", "Config.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(configFile);

    @Getter
    private static String pr = "§7[§cSkyStriker§7] §b";

    @Override
    public void loadDBTables() {
        getSql().getDaoList().registerDao("skystriker_players", true, SkyStrikerPlayer.class);
        skyStrikerPlayerHandler = new SkyStrikerPlayerHandler(getSql());
    }

    @Override
    public void onAfterEnable() {
        instance = this;
        registerEvents();

        config.saveConfig();
    }

    @Override
    public void onDisable() {}

    @Override
    public SQLConfiguration getSQLConfiguration() {
        return Bedrock.getInstance().getSQLConfiguration();
    }

    @Override
    public String getLanguagePackName() {
        return "skystriker";
    }


    public static SkyStrikerPlayerHandler getSkyStrikerPlayerHandler() {
        return skyStrikerPlayerHandler;
    }

    public static HashMap<UUID, Manager> getStats() {
        return stats;
    }

    public static main getInstance() {
        return instance;
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
    }

}
