package de.theheroghost.main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Teams {
    /*
    Für die Teams extra Configs Anlegen
    Spieler in der Config Speicher mit ArenaID und Team!
     */

    public static File SpielerFile = new File("plugins/SkyStriker/Spieler/",  "Spieler.yml");
    public static FileConfiguration Spieler = YamlConfiguration.loadConfiguration(SpielerFile);

    public static void save() throws IOException {
        try {
            Spieler.save(SpielerFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveTeams() {
        try {
            Teams.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
