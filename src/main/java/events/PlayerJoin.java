package events;

import de.theheroghost.main.main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import sql.Manager;

public class PlayerJoin implements Listener {

    @EventHandler
    public void on(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        new Manager(player.getUniqueId()).load();
    }
}
